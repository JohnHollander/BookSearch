﻿using BooksWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace BooksWebPortal.ExternalBookService
{
    public class BookService
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public List<BookModel> Search(string searchString, int searchAttribute)
        {
            var uri = GenerateSearchUri(searchString, searchAttribute);
            var response = _httpClient.GetAsync(uri).Result;
            var list = response.Content.ReadAsAsync<List<BookModel>>().Result;
            return list;
        }

        private Uri GenerateSearchUri(string searchString, int searchAttribute)
        {
            var baseUri = ConfigurationManager.AppSettings["BookApiEndpoint"];
            return new Uri(baseUri + string.Format("api/BookSearch/Search?searchString={0}&searchAttribute={1}", searchString, searchAttribute));
        }
    }
}