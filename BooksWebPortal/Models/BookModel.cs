﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BooksWebPortal.Models
{
    public class BookModel
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public decimal Price { get; set; }
        public DateTime Publish_date { get; set; }
        public string Description { get; set; }
    }
}