﻿using BooksWebPortal.ExternalBookService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BooksWebPortal.Controllers
{
    public class BookController : Controller
    {
        //
        // GET: /Book/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchBooks(string searchString, int searchAttribute)
        {
            if (searchString != String.Empty)
            {
                var bookService = new BookService();
                ViewData.Model = bookService.Search(searchString, searchAttribute);
            }
            return View("Index");
        }
    }
}
