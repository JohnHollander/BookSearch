﻿namespace BookService
{
    public enum SearchAttribute
    {
        Title = 1,
        Author = 2,
        TitleAndDescription = 3
    }
}
