﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BookService
{
    public class BookCatalogReader
    {
        public IEnumerable<Book> GetBookCatalog()
        {
            var serializer = new XmlSerializer(typeof(Catalog));
            using (var fileStream = new FileStream(@"../../BookData/Books.xml", FileMode.Open))
            {
                var catalog = (Catalog)serializer.Deserialize(fileStream);
                return catalog.Books;
            }
        }
    }
}
