﻿using System;
using System.Xml.Serialization;

namespace BookService
{
    public class Book
    {
        [XmlElement("author")]
        public string Author { get; set; }
        [XmlElement("title")]
        public string Title { get; set; }
        [XmlElement("genre")]
        public string Genre { get; set; }
        [XmlElement("price")]
        public decimal Price { get; set; }
        [XmlElement("publish_date")]
        public DateTime Publish_date { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
    }
}
