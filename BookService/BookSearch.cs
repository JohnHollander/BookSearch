﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookService
{
    public class BookSearch
    {
        private int exactFraseScoreAdd = 10;
        private int nrOfSearchResults = 5;
        private int minWordLength = 3;
        private List<Book> _catalog;
        private List<string> _ignoreWords;

        public BookSearch()
        {
            var bookCatalogReader = new BookCatalogReader();
            this._catalog = bookCatalogReader.GetBookCatalog().ToList();
            this._ignoreWords = GetIgnoreWords().ToList();
        }

        public List<Book> SearchBooks(string searchString, SearchAttribute searchAttribute)
        {
            var searchStringWords = searchString.Split(' ');
            var searchMatches = SearchMatchingBooks(_catalog, searchStringWords, searchAttribute);
            var chosenForDisplay = ChosenSearchResult(searchMatches).ToList();

            return chosenForDisplay;
        }

        private Dictionary<Book, decimal> SearchMatchingBooks(List<Book> catalog, string[] searchStringWords, SearchAttribute searchAttribute)
        {
            var searchMatches = new Dictionary<Book, decimal>();

            foreach (var book in catalog)
            {
                var bookKeyWords = GetBookKeyWords(book, searchAttribute);
                var matchPoints = CompareFrases(searchStringWords, bookKeyWords);

                if(matchPoints != 0)
                    searchMatches.Add(book, matchPoints);
            }

            return searchMatches;
        }

        //Returns an array of words gathered from the books attributes depending on 
        private string[] GetBookKeyWords(Book book, SearchAttribute searchAttribute)
        {
            switch (searchAttribute)
            { 
                case SearchAttribute.Title:
                    return book.Title.Split(' ');
                case SearchAttribute.Author:
                    return book.Author.Split(' ');
                case SearchAttribute.TitleAndDescription:
                    return book.Title.Split(' ').Concat(book.Description.Split(' ')).ToArray();
                default:
                    return new string[0];
            }
        }

        //Returns the highest ranked results
        private List<Book> ChosenSearchResult(Dictionary<Book, decimal> orderItems)
        {
            return orderItems.OrderByDescending(entry => entry.Value)
                    .Take(nrOfSearchResults)
                    .Select(d => d.Key).ToList();
        }

        //Finds matching words in two frases
        private decimal CompareFrases(string[] firstFrase, string[] secondFrase)
        {
            var fraseMatchScore = 0;
            var isExactFrase = true;

            for (int i = 0; i < firstFrase.Length; i++)
            {
                var word = firstFrase[i];
                for (int j = 0; j < secondFrase.Length; j++)
                {
                    var otherWord = secondFrase[j];
                    var wordMatchScore = 0;

                    if (!IgnoreWord(word, otherWord))
                    {
                        wordMatchScore = CompareWords(word, otherWord);
                        fraseMatchScore += wordMatchScore;
                    }

                    if (i == j && wordMatchScore < 2)
                        isExactFrase = false;
                }
            }

            if (isExactFrase)
                fraseMatchScore += exactFraseScoreAdd;

            return fraseMatchScore;
        }

        //Ignore stopwords and short words
        private bool IgnoreWord(string word, string otherWord)
        {
            if (otherWord.Length < minWordLength)
                return true;

            foreach (var ignoreWord in _ignoreWords)
            {
                if (CompareWords(word, ignoreWord) == 2 || CompareWords(otherWord, ignoreWord) == 2)
                    return true;
            }

            return false;
        }

        //If both words are part of each other they are the same word and score 2 points
        private int CompareWords(string word, string otherWord)
        {
            var wordMatchScore = 0;
            var culture = CultureInfo.CurrentCulture;

            if (culture.CompareInfo.IndexOf(word, otherWord, CompareOptions.IgnoreCase) >= 0)
                wordMatchScore++;
            if (culture.CompareInfo.IndexOf(otherWord, word, CompareOptions.IgnoreCase) >= 0)
                wordMatchScore++;

            return wordMatchScore;
        }

        private IEnumerable<string> GetIgnoreWords()
        {
            return new List<string> 
            { 
                "I", "a","an","and","are","as","at","be","by","com","for","how","in","is","it","of","on","or","that",
                "the","this","to","was","what","when","who","will","with"
            };
        }
    }
}
