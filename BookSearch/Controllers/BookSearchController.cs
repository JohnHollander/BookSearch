﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BookService;

namespace BookApi.Controllers
{
    public class BookSearchController : ApiController
    {
        [HttpGet]
        public List<Book> SearchBook(string searchString, int searchAttribute)
        {
            var bookSearch = new BookSearch();
            return bookSearch.SearchBooks(searchString, (SearchAttribute)searchAttribute);
        }
    }
}
