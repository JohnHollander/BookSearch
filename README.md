BookSearch.

For simplicity, both BookApi- and the BooksWebPortal projects are in the same solution
though they could be separate.  The BookService class library contains the search engine 
and is used by the BookApi project.

Instructions

- Create folder c:\BookData and put the Books.xml file in it.
- Open the BookSearch solution in Visual studio
- In "Set startup projects", set BookApi to run first and BooksWebPortal second
- Click run